include ${FSLCONFDIR}/default.mk

PROJNAME = first
XFILES   = first first_utils first_mult_bcorr
SCRIPTS  = run_first run_first_all first_flirt \
           concat_bvars first_roi_slicesdir first_boundary_corr \
           surface_fdr first3Dview
LIBS     = -lfsl-shapeModel -lfsl-first_lib -lfsl-vtkio -lfsl-meshclass \
           -lfsl-newimage -lfsl-miscmaths -lfsl-NewNifti -lfsl-cprob \
           -lfsl-znz -lfsl-utils

all: ${XFILES}

%: %.o
	${CXX} ${CXXFLAGS} -o $@ $^ ${LDFLAGS}
